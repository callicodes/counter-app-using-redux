import logo from './logo.svg';
import './App.css';
import { increment, decrement } from './redux'
import {connect} from "react-redux";

function App(props) {    
  return (
      <div>
          <h1>{props.count}</h1>
          <button onClick={props.decrement}>-</button>
          <button onClick={props.increment}>+</button>
      </div>
  )
}

// function mapStateToProps(globalState){

//  return {
//     count: globalState
//  }
// }



export default connect(state => ({count: state}), {increment, decrement})(App)
